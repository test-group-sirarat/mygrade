       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-GRADE-TO-AVG.
       AUTHOR. SIRARAT.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
           ORGANIZATION IS LINE SEQUENTIAL.
       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  GRADE-DETAIL.
           88 END-OF-MYGRADE-FILE   VALUE  HIGH-VALUE.
           05 STU-ID PIC 9(6).
           05 COUSE-NAME PIC X(50).
           05 CREDIT PIC 9(1).
           05 GRADE  PIC  X(2).

       FD  AVG-FILE.
       01  AVG-DETAIL.
           05 AVG-GRADE     PIC 9V9(3).
           05 AVG-SCI-GRADE PIC 9V9(3).
           05 AVG-CS-GRADE  PIC 9V9(3).
       WORKING-STORAGE SECTION. 
       01  GRADE-NUM         PIC 9(3)V9(3).
       01  GRADE-AVG         PIC 9(3)V9(3).
       01  CREDIT-NUM        PIC 9(3)V9(3).
       01  SUM-GRADE        PIC 9(3)V9(3).
       01  STU-ID-SUB        PIC X.
       01  STU-ID-SUB2        PIC X(2).
       01  SUM-SCI-GRADE      PIC 9(3)V9(3).
       01  SUM-CS-GRADE       PIC 9(3)V9(3).
       01  CREDIT-SCI-GRADE       PIC 9(3)V9(3).
       01  CREDIT-CS-GRADE       PIC 9(3)V9(3).
      
       PROCEDURE DIVISION.
       000-BEGIN.
           OPEN INPUT GRADE-FILE 
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-MYGRADE-FILE 
              READ GRADE-FILE 
                 AT END SET END-OF-MYGRADE-FILE TO TRUE
              END-READ
              IF NOT END-OF-MYGRADE-FILE THEN 
              MOVE STU-ID TO  STU-ID-SUB
              MOVE STU-ID TO  STU-ID-SUB2
                 PERFORM 001-PROCESS THRU 001-EXIT
              END-IF 
           END-PERFORM
           DISPLAY "AVG = "  AVG-GRADE 
           DISPLAY "AVG-SCI-GRADE= " AVG-SCI-GRADE  
           DISPLAY "AVG-CS-GRADE  = "AVG-CS-GRADE 
           WRITE AVG-DETAIL 
           CLOSE GRADE-FILE 
           CLOSE AVG-FILE 
           GOBACK 
           .
       001-PROCESS.
           EVALUATE GRADE  
             WHEN  "A" MOVE 4 TO GRADE-NUM 
             WHEN  "B+" MOVE   3.5 TO  GRADE-NUM
             WHEN  "B" MOVE 3 TO GRADE-NUM 
             WHEN  "C+" MOVE   2.5 TO  GRADE-NUM
             WHEN  "C" MOVE   2 TO  GRADE-NUM
             WHEN  "D+" MOVE   1.5 TO  GRADE-NUM
             WHEN  "D" MOVE   1 TO  GRADE-NUM
            END-EVALUATE 
           COMPUTE CREDIT-NUM = CREDIT-NUM  + CREDIT
           COMPUTE SUM-GRADE = SUM-GRADE + (GRADE-NUM * CREDIT )
           COMPUTE AVG-GRADE = SUM-GRADE / CREDIT-NUM 
           IF STU-ID-SUB  EQUAL "3"  THEN
              COMPUTE CREDIT-SCI-GRADE = CREDIT-SCI-GRADE + CREDIT
              COMPUTE  SUM-SCI-GRADE =SUM-SCI-GRADE + 
      -     (GRADE-NUM * CREDIT )
              COMPUTE AVG-SCI-GRADE  = SUM-SCI-GRADE/ CREDIT-SCI-GRADE 
           END-IF 
           IF STU-ID-SUB2  EQUAL "31"  THEN
              COMPUTE CREDIT-CS-GRADE = CREDIT-CS-GRADE + CREDIT
              COMPUTE  SUM-CS-GRADE  =SUM-CS-GRADE + 
      -     (GRADE-NUM * CREDIT )
              COMPUTE AVG-CS-GRADE  = SUM-CS-GRADE/ CREDIT-CS-GRADE 
           END-IF 

           .
       001-EXIT.
           EXIT.

         